DB_HOST_ALLOCATOR=$1 #allocator_test  
DB_HOST_CORE=$1 #Core service test
DB_HOST_AVIATOR=$1 #Aviator_test

DB_USERNAME=user
DB_PASSWORD=password
DBS_ALLOCATOR=("db_name_$3")
location=$2
TODAY=$(date +"%Y%m%d")
DUMP_LOCATION="./$2/${TODAY}"
DATETIME_NOW=$(date +"%Y%m%d%H%M%S")

mkdir -p $DUMP_LOCATION

for db in "${DBS_ALLOCATOR[@]}"; do
	  echo "Dumping ${db}"
	    mysqldump --host="${DB_HOST_ALLOCATOR}" -u"${DB_USERNAME}" -p${DB_PASSWORD} --routines --set-gtid-purged=OFF "${db}" > ${DUMP_LOCATION}/${DATETIME_NOW}_${db}_dump.sql
	      echo "--------"       
      done
