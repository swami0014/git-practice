if [[ ! ($1 && $2 && $3 ) ]]; then
    echo "Correct Usage : import_db.sh <source directory> <db extension>"
        exit
fi

DB_HOST_ALLOCATOR=$3  #Allocator
DB_HOST_CORE=$3  #core service
DB_HOST_AVIATOR=$3   #Aviator

        


DB_USERNAME=user
DB_PASSWORD=password

DBS_ALLOCATOR=("Db Name")

DUMP_LOCATION=$1
DB_EXT=$2

for db in "${DBS_CORE[@]}"; do
	  db_name=${db}_${DB_EXT}
	    echo "Importing ${db_name}"
	      mysql --host=${DB_HOST_CORE} -u${DB_USERNAME} -p${DB_PASSWORD} -e "drop database if exists ${db_name}; create database ${db_name}"
	        find ${DUMP_LOCATION} -name "*${db}*.sql" | awk '{ print "source",$0 }' | mysql --batch --host=$DB_HOST_CORE ${db_name}  -u${DB_USERNAME} -p${DB_PASSWORD}
		  echo "--------"
	  done
