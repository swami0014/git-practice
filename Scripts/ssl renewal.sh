#!/bin/bash
HOST="db"
while ! mysqladmin ping -h $HOST --silent; do
    sleep 10
    echo "adserve"
done
pip install newrelic
newrelic-admin generate-config f2bbfd489c817c55871e0534d7f1021524225a5b  newrelic.ini
sed -i 's/Python Application/Core_ACM_IPG/g' newrelic.ini
NEW_RELIC_CONFIG_FILE=newrelic.ini newrelic-admin run-program /usr/bin/python manage.py migrate
NEW_RELIC_CONFIG_FILE=newrelic.ini newrelic-admin run-program /usr/bin/python manage.py runserver 0.0.0.0:$1
