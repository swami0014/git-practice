if [[ ! ($1 && $2  && $3)  ]]; then
  echo "Correct Usage : filename.sh <branch_name> <Release_json> <branch_tag> "
exit
fi

cd skynetplatform

git checkout dev
git pull  origin dev 


git add .
git checkout "$1" 
git pull origin "$1"
git merge dev 


cd SKYNET  

echo "{
 "$2"
}" > release-map.json #2 is json file

cd ../..

python compare_json_update.py "$1"

cd skynetplatform

git add --all
git commit -m "$(date +"%d-%m-%Y")"
git tag -a -m "$(date +"%d-%m-%Y")" "$1"-Skynet-"$3"  # 3 is tag for tat
git push origin "$1" --tags


echo done
