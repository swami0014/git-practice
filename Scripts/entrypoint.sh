# Author : Roopak A N <roopak@surewaves.com>

if [[ -z $INCLUDE_PATH ]]; then
  INCLUDE_PATH="../../deployer/include/"
fi

if [[ -z $PLUGIN_PATH ]]; then
  PLUGIN_PATH="../../deployer/plugins/"
fi

if [[ -z $POKEME_PATH ]]; then
  POKEME_PATH="../../deployer/"
fi

source "${INCLUDE_PATH}/color-codes.sh" || (echo "Could not load color-codes.sh" && exit)
source "${INCLUDE_PATH}/utils.sh" || (echo "Could not load utils.sh" && exit)

setup_service()
{
  local SVC=$1
  local service_name=$(get_value_from_json "$SVC" ".name")
  
  message "Deploying $service_name" "U"

  local service_config_folder="${TMP_DIR}/${service_name}/config"
  mkdir -p ${service_config_folder}

  local server_config_name=$(get_value_from_json "$SVC" ".server_config")
  local server_config=$(get_value_from_json "$SERVER_CONFIGS" ".$server_config_name")
  local server_username=$(get_value_from_json "${server_config}" ".username")
  local server_host=$(get_value_from_json "${server_config}" ".host")
  local ssh_key=$(get_value_from_json "${server_config}" ".ssh_file")

  substitute_env "${ENV_SH_FILE}" "${TEMPLATE_FOLDER}/.env.template" "${service_config_folder}/.env"

  setup_core_docker_compose "$SVC" || error_exit "Could not setup docker-compose or keycloak file"

  message "Creating folder"
  ssh -o ConnectTimeout=30 -t  ${server_username}@${server_host} /bin/bash  << EOF || error_exit "creating based folder failed for $service_name"
    mkdir -p ./${RELEASE_ID}/$service_name/
    cd ./${RELEASE_ID}/$service_name/
    docker-compose down
    sudo chown ${server_username}:${server_username} . -R
EOF

  message "Copying configs and docker-compose"
  scp -pr  "${TMP_DIR}/${service_name}/" ${server_username}@${server_host}:./${RELEASE_ID}/ || error_exit "copying failed for ${service_name}"

  message "Trying to start service : ${service_name}"
  ssh -o ConnectTimeout=30 -t  ${server_username}@${server_host} /bin/bash  << EOF || error_exit "ssh execution failed for $service_name"
    cd ./${RELEASE_ID}/$service_name/
    mkdir -p ./env
    sudo gcloud docker pull "${BIZ_AVIATOR_AUDIT_DOCKER_IMAGE}"
    sudo docker-compose up -d
EOF

  deployed_success "$service_name"
}

setup_core_docker_compose()
{
  local SVC=$1

  local docker_compose_file=${TMP_DIR}/${service_name}/docker-compose.yml

  #local keycloak_json=${TMP_DIR}/${service_name}/config/keycloak.json

  message "Creating docker-compose for $service_name"
  substitute_env "$ENV_SH_FILE" "${TEMPLATE_FOLDER}/docker-compose.yml.template" "$docker_compose_file" \
    && message_success "docker-compose file creation successful." \
    || error_exit "docker-compose file creation failed."

    #substitute_env "$ENV_SH_FILE" "${TEMPLATE_FOLDER}/keycloak.json.template" "$keycloak_json" \
    #&& message_success "Keycloak json created successfully." \
    #|| error_exit "Keycloak json file creation failed."
}

get_core_arguements(){
  while [[ $# -gt 0 ]]
    do
      key="$1"
      case $key in
          -h|--help)
          echo "
Usage: poke-me.sh [options] 

Options
-------
-h, --help         : Show this message.
          "
          exit
          ;;
          *)
          ;;
      esac
      shift 
    done
}

message "Poke-Me - SureLabs Deployer." "U"

get_core_arguements $@

SERVICE_ID="BIZ_AVIATOR_AUDIT"
TEMPLATE_FOLDER="./template/"

ENV_FILE_NAME="./env.json"
if [[ ! -a $ENV_FILE_NAME ]]; then
  ls -al
  error_exit "Environment file - '${ENV_FILE_NAME}' - does not exist"
fi

CONFIG_FILE_NAME="./config.json"
if [[ ! -a $CONFIG_FILE_NAME ]]; then
  error_exit "Environment file - '${CONFIG_FILE_NAME}' - does not exist"
fi

ENV_SH_FILE="./env.sh"
if [[ ! -a $ENV_SH_FILE ]]; then
  error_exit "Environment Shell file - '${ENV_SH_FILE}' - does not exist"
fi

message "Reading Environment file."

ENV_FILE_CONTENTS=$(cat ${ENV_FILE_NAME})
CONFIG_FILE_CONTENTS=$(cat ${CONFIG_FILE_NAME})

RELEASE_ID=$(get_value_from_json "$ENV_FILE_CONTENTS" ".release_id")

SERVICES=$(get_value_from_json "$ENV_FILE_CONTENTS" ".services")
SERVER_CONFIGS=$(get_value_from_json "$ENV_FILE_CONTENTS" ".servers")
DB_CONFIGS=$(get_value_from_json "$ENV_FILE_CONTENTS" ".dbs")

TMP_DIR="./tmp/$RELEASE_ID"
mkdir -p $TMP_DIR

SVC=$(get_value_from_json "${SERVICES}" ".${SERVICE_ID}")

setup_service "$SVC" && \
  message_success "Deployment successful." || \
  error_exit "Deployment failed."

read -n1 -r -p "$(message "Press any key to exit.")" key
printf "\n\n"

toilet -f mono12 -F metal "ThugLife"

printf "\n\n"