images=("advertiser"  "advertiser-audit"  "as-runlog-adapter"  "asrunlog_adapter3.1.1_dt2017.03.01"  "atc"  "automation" "automation_api"  "aviator"  "aviator-audit"  "biz-dependenies-cache"  "common-service"  "core-adapter"  "core-adapters"  "core-email"  "core-service-acm"  "core-service-ad-creative-management"  "core-service-ad-reporting"  "core-service-advertiser"  "core-service-audience"  "core-service-audience-measurement"   "core-service-channel"  "core-service-channel-test"  "core-service-deployer"  "core-service-reporter"  "cyborg"  "data-channel-prediction-h2o"  "data-dpe"  "email-factory"  "finance"  "letterbox"  "navigator-propeller"  "skynet-biz-db-migration"  "stockist"  "terminator"  "test"  "test-core-adapters"  "traffice_manager" )
images1=( "ad-detection" "ad-detection-service" "as-run-logs" "as-run-logs-api" "asrunlog_adapater_dev" "asrunlog_adapter" "asrunlog_adapter_dev" "asrunlog_adapterdev3.1.1_dt2017.02.13" "asrunlogs_api" "atc" "atc-finance" "atc_finance_final" "atc_head" "atc_test" "aviator-app" "aviator-app-dev" "aviator-app-test" "aviator-audit-trail"  "core-adapter" "core-adapters" "core-service-ad-creative-deployment" "core-service-ad-creative-management" "core-service-ad-reporting" "core-service-advertiser" "core-service-audience-measurement"  "core-service-channel"  "finance" "kube_testing" "login_allocator" "mock-server" "navigator-api" "skynet" "skynet-app" "skynet-app-tg" "skynet_16_12_9_0" "skynet_allocator" "skynet_final" "skynet_test_16_11_24_0" "terminator" "v16.10.28.0")

#For gcr.io
for image in ${images[@]};
do
echo "For image = $image"
y=$3
m=$2
d=$1
last=2016
flag=1
date=$y-$m-$d
echo $date
list_images=`gcloud  container images list-tags gcr.io/surewaves-test/$image --filter="timestamp.datetime < $date" --format='get(digest)'`
for j in $list_images
do
IMAGE="gcr.io/surewaves-test/$image"
gcloud  container images delete --force-delete-tags   -q $IMAGE@$j 
#echo "$j image is deleteed"
done
m=`expr $m - $flag`
d=31
while [ $flag ]
do
if [ $y -lt $last ]
then
break
fi
while [ $m -ge $flag ]
do
if [ $m -lt 10 ]
then
m=0$m
fi
date=$y-$m-$d
list_images=`gcloud container images list-tags gcr.io/surewaves-test/$image --filter="timestamp.datetime < $date" --format='get(digest)'`
echo "$date"
for j in $list_images
do
IMAGE="gcr.io/surewaves-test/$image"
gcloud  container images delete  --force-delete-tags -q $IMAGE@$j
#echo "$j images is deleted"
done
m=`expr $m - $flag`
done
m=12
y=`expr $y - $flag`
done
done

#FOR Asia.gcr.io
echo "cleaning Asia.gcr.io"
for image in ${images1[@]};
do
echo "For image = $image"
y=$3
m=$2
d=$1
flag=1
date=$y-$m-$d
echo $date
list_images=`gcloud container images list-tags asia.gcr.io/surewaves-test/$image --filter="timestamp.datetime < $date" --format='get(digest)'`
for j in $list_images
do
IMAGE="asia.gcr.io/surewaves-test/$image"
gcloud  container images delete  --force-delete-tags  -q $IMAGE@$j
#echo "$j image is deleted"
done
m=`expr $m - $flag`
d=31
while [ $flag ]
do
if [ $y -lt $last ]
then
break
fi
while [ $m -ge $flag ]
do
if [ $m -lt 10 ]
then
m=0$m
fi
date=$y-$m-$d
list_images=`gcloud container images list-tags asia.gcr.io/surewaves-test/$image --filter="timestamp.datetime < $date" --format='get(digest)'`
echo "$date"
for j in $list_images
do
IMAGE="asia.gcr.io/surewaves-test/$image"
gcloud  container images delete  --force-delete-tags  -q $IMAGE@$j
#echo "$j Image is Deleted."
done
m=`expr $m - $flag`
done
m=12
y=`expr $y - $flag`
done
done

